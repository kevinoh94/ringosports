package com.example.ringosports.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Spinner
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ringosports.R
import com.example.ringosports.models.RingoDevice
import com.example.ringosports.views.device.DeviceListFragment
import kotlinx.android.synthetic.main.device_card.view.*
import kotlinx.android.synthetic.main.trainee_list_item.view.name

class DeviceListAdapter (
    private val parentActivity: DeviceListFragment,
    private val ringoDevices: ArrayList<RingoDevice>
) :
    RecyclerView.Adapter<DeviceListAdapter.ViewHolder>() {

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val name: TextView = v.name
        val color: Spinner = v.color
        val battery : TextView = v.battery
        val sensor : TextView = v.sensor
        val rfid : TextView = v.rfid
        val reset : Button = v.reset
        val switch : Switch = v.led_switch
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.device_card, viewGroup, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(v: ViewHolder, position: Int) {
        val device = ringoDevices[position]
        v.name.text = device.name
        if (device.colorAdapter == null) {
            parentActivity.setColorAdapter(device)
        }
        v.color.adapter = device.colorAdapter
        v.battery.text = device.battery
        v.sensor.text = Integer.toString(device.count)
        if (device.rfid != null) {
            v.rfid.visibility = View.VISIBLE
            v.rfid.text = device.rfid
        }

        v.reset.setOnClickListener {
            device.reset()
            v.sensor.text = Integer.toString(device.count)
        }

        v.switch.setOnCheckedChangeListener { _, isChecked ->
           parentActivity.sendLed(device.connHandle, isChecked)
        }

    }

    override fun getItemCount() = ringoDevices.size

}
