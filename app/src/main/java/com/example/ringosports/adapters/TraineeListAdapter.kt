package com.example.ringosports.adapters

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.ringosports.models.Trainee
import androidx.recyclerview.widget.RecyclerView
import com.example.ringosports.R
import com.example.ringosports.views.results.ResultsDetailFragment
import com.example.ringosports.views.results.ResultsListActivity
import kotlinx.android.synthetic.main.trainee_list_item.view.*

class TraineeListAdapter(
    private val parentActivity: ResultsListActivity?,
    private val trainees: ArrayList<Trainee>,
    private val twoPane: Boolean
) :
    RecyclerView.Adapter<TraineeListAdapter.ViewHolder>() {

    private val onClickListener: View.OnClickListener

    init {
        onClickListener = View.OnClickListener { v ->
            val trainee = v.tag as Trainee
            if (twoPane) {
                val fragment = ResultsDetailFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ResultsDetailFragment.ARG_TRAINEE_ID, trainee)
                    }
                }
                parentActivity!!.supportFragmentManager
                    .beginTransaction().run {
                        replace(R.id.results_detail_container, fragment)
                        commit()
                    }
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.trainee_list_item, viewGroup, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val trainee = trainees[position]
        viewHolder.nameView.text = trainee.name
        viewHolder.tagView.text = trainee.tag

        with(viewHolder.itemView) {
            tag = trainee
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = trainees.size

    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val nameView: TextView = v.name
        val tagView : TextView = v.tagId
    }

}