package com.example.ringosports.models

import android.widget.ArrayAdapter

data class RingoDevice(val name: String, val connHandle: Int, var colorAdapter : ArrayAdapter<String>?,
                       var battery : String, var sensor : String, var rfid : String?, var count : Int) {
    constructor(name: String, connHandle: Int) : this(name, connHandle, null, "100%", "none", null, 0)

    fun count() {
        count += 1
    }

    fun reset() {
        count = 0
    }
}