package com.example.ringosports.models

import java.io.Serializable

data class Trainee(val name: String, val tag: String, val results: String?) : Serializable

