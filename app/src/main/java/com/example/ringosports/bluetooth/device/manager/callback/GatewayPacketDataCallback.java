package com.example.ringosports.bluetooth.device.manager.callback;

import android.bluetooth.BluetoothDevice;
import androidx.annotation.NonNull;
import no.nordicsemi.android.ble.callback.profile.ProfileDataCallback;
import no.nordicsemi.android.ble.data.Data;

@SuppressWarnings("ConstantConditions")
public abstract class GatewayPacketDataCallback implements ProfileDataCallback, GatewayPacketCallback {

    @Override
    public void onDataReceived(@NonNull final BluetoothDevice device, @NonNull final Data data) {
        if (data.size() < 1) {
            onInvalidDataReceived(device, data);
            return;
        }
        onDataUpdate(device, data);
    }
}
