package com.example.ringosports.bluetooth.device.manager.callback;

import android.bluetooth.BluetoothDevice;
import androidx.annotation.NonNull;
import no.nordicsemi.android.ble.data.Data;

public interface GatewayPacketCallback {

    /**
     * Called when a Ringo Device connection is lost.
     *
     * @param device
     * @param connected true if device connected, false if device disconnected
     */
//    void onDeviceConnectionNotification(@NonNull final BluetoothDevice device, boolean connected);

    /**
     * Called when data update packet is received. Notification.
     *
     * @param device
     * @param data passes along the data received for handling
     */
    void onDataUpdate(@NonNull final BluetoothDevice device, Data data);
}
