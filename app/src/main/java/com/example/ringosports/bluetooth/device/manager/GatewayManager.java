/*
 * Copyright (c) 2018, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.example.ringosports.bluetooth.device.manager;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import com.example.ringosports.bluetooth.device.manager.callback.GatewayPacketDataCallback;
import com.example.ringosports.bluetooth.device.manager.callback.RingoLED;
import com.example.ringosports.bluetooth.device.manager.callback.RingoLedDataCallback;
import com.example.ringosports.models.RingoDevice;
import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.data.Data;

import java.util.UUID;

public class GatewayManager extends BleManager<GatewayManagerCallbacks> {
	/** RingoGateway Service UUID */
	//public final static UUID GATEWAY_UUID_SERVICE = UUID.fromString("3e520001-1368-b682-4440-d7dd234c45bc");
	/** RingoPlay LED service UUID */
	public final static UUID RINGO_UUID_SERVICE = UUID.fromString("3e520001-1368-b682-4440-d7dd234c45bc"); //UUID.fromString("3c984019-5f14-11e9-8647-d663bd873d93");
	/** Gateway Packet Characteristic UUID */
	//public final static UUID PACKET_UUID_CHAR = UUID.fromString("3e520003-1368-b682-4440-d7dd234c45bc");
	/** BUTTON characteristic UUID. */
	private final static UUID LBS_UUID_BUTTON_CHAR = UUID.fromString("3e520003-1368-b682-4440-d7dd234c45bc"); //UUID.fromString("00001524-1212-efde-1523-785feabcd123");
	/** RingoPlay LED characteristic UUID */
	private final static UUID RINGO_UUID_LED_CHAR = UUID.fromString("3e520002-1368-b682-4440-d7dd234c45bc");
	/** RingoPlay LED pattern characteristic UUID */
	//private final static UUID RINGO_UUID_PATTERN_CHAR = UUID.fromString("3c984021-5f14-11e9-8647-d663bd873d93");

	private BluetoothGattCharacteristic mPacketCharacteristic, mButtonCharacteristic, mLedCharacteristic;
	private boolean mSupported;
	private boolean mLedOn;

	public GatewayManager(@NonNull final Context context) {
		super(context);
	}

	@NonNull
	@Override
	protected BleManagerGattCallback getGattCallback() {
		return mGattCallback;
	}

	@Override
	protected boolean shouldClearCacheWhenDisconnected() {
		return !mSupported;
	}

	/**
	 * The Button callback will be notified when a notification from Button characteristic
	 * has been received, or its data was read.
	 * <p>
	 */

	private final GatewayPacketDataCallback mPacketCallback = new GatewayPacketDataCallback() {
		@Override
		public void onDataUpdate(@NonNull BluetoothDevice device, Data data) {
			mCallbacks.onDataUpdate(device, data);
		}

		@Override
		public void onInvalidDataReceived(@NonNull BluetoothDevice device, @NonNull Data data) {
			log(Log.WARN, "Invalid data received " + data);
		}
	};

	/**
	 * The LED callback will be notified when the LED state was read or sent to the target device.
	 * <p>
	 * This callback implements both {@link no.nordicsemi.android.ble.callback.DataReceivedCallback}
	 * and {@link no.nordicsemi.android.ble.callback.DataSentCallback} and calls the same
	 * method on success.
	 * <p>
	 */
	private final RingoLedDataCallback mLedCallback = new RingoLedDataCallback() {
		@Override
		public void onLedStateChanged(@NonNull final BluetoothDevice device,
									  final boolean on) {
			mLedOn = on;
			mCallbacks.onLedStateChanged(device, on);
		}

		@Override
		public void onInvalidDataReceived(@NonNull final BluetoothDevice device,
										  @NonNull final Data data) {
			// Data can only invalid if we read them. We assume the app always sends correct data.
			log(Log.WARN, "Invalid data received: " + data);
		}
	};

	/**
	 * BluetoothGatt callbacks object.
	 */
	private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {
		@Override
		protected void initialize() {
			setNotificationCallback(mButtonCharacteristic).with(mPacketCallback); //with(mButtonCallback);
			readCharacteristic(mLedCharacteristic).with(mLedCallback).enqueue();
			readCharacteristic(mButtonCharacteristic).with(mPacketCallback).enqueue(); //with(mButtonCallback).enqueue();
			enableNotifications(mButtonCharacteristic).enqueue();
		}

		@Override
		public boolean isRequiredServiceSupported(@NonNull final BluetoothGatt gatt) {
			final BluetoothGattService service = gatt.getService(RINGO_UUID_SERVICE); //(LBS_UUID_SERVICE);
			if (service != null) {
				mButtonCharacteristic = service.getCharacteristic(LBS_UUID_BUTTON_CHAR);
				mLedCharacteristic = service.getCharacteristic(RINGO_UUID_LED_CHAR);  //(LBS_UUID_LED_CHAR);
			}

			boolean writeRequest = false;
			if (mLedCharacteristic != null) {
				final int rxProperties = mLedCharacteristic.getProperties();
				writeRequest = (rxProperties & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0;
			}

			mSupported = mButtonCharacteristic != null && mLedCharacteristic != null && writeRequest;
			return true;//mSupported;
		}

		@Override
		protected void onDeviceDisconnected() {
			mButtonCharacteristic = null;
			mLedCharacteristic = null;
		}
	};

	/**
	 * Sends a request to the device to turn the LED on or off.
	 *
	 * @param on true to turn the LED on, false to turn it off.
	 */
	public void send(int connHandle, final boolean on) {
		// Are we connected?
		if (mLedCharacteristic == null)
			return;

		// No need to change?
//		if (mLedOn == on)
//			return;

		// 0x02 01 00 00
		byte[] id = intToByteArray(connHandle);
		byte[] data = {0x02, on ? RingoLED.turnOn() : RingoLED.turnOff(), id[0], id[1] };
		writeCharacteristic(mLedCharacteristic, new Data(data))
				.with(mLedCallback).enqueue();
	}

	private static byte[] intToByteArray(int value) {
		return new byte[] {
//				(byte)(value >>> 24),
//				(byte)(value >>> 16),
				(byte)(value >>> 8),
				(byte)value};
	}
}
