/*
 * Copyright (c) 2018, Nordic Semiconductor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.example.ringosports.bluetooth.device.manager;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.ringosports.R;
import com.example.ringosports.bluetooth.scanning.DiscoveredBluetoothDevice;
import com.example.ringosports.models.RingoDevice;
import no.nordicsemi.android.ble.data.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GatewayViewModel extends AndroidViewModel implements GatewayManagerCallbacks {
	private final GatewayManager gatewayManager;
	private BluetoothDevice mDevice;
	private ArrayList<RingoDevice> ringos;

	// Connection states Connecting, Connected, Disconnecting, Disconnected etc.
	private final MutableLiveData<String> mConnectionState = new MutableLiveData<>();

	// Flag to determine if the device is connected
	private final MutableLiveData<Boolean> mIsConnected = new MutableLiveData<>();

	// Flag to determine if the device has required services
	private final MutableLiveData<Boolean> mIsSupported = new MutableLiveData<>();

	// Flag to determine if the device is ready
	private final MutableLiveData<Void> mOnDeviceReady = new MutableLiveData<>();

	// Flag that holds the on off state of the LED. On is true, Off is False
	private final MutableLiveData<Boolean> mLEDState = new MutableLiveData<>();

	// Flag that holds the pressed released state of the button on the devkit.
	// Pressed is true, Released is false
	private final MutableLiveData<Boolean> mButtonState = new MutableLiveData<>();

	private final MutableLiveData<Boolean> mDeviceState = new MutableLiveData<>();

	public LiveData<Void> isDeviceReady() {
		return mOnDeviceReady;
	}

	public LiveData<String> getConnectionState() {
		return mConnectionState;
	}

	public LiveData<Boolean> isConnected() {
		return mIsConnected;
	}

	public LiveData<Boolean> getButtonState() {
		return mButtonState;
	}

	public LiveData<Boolean> getDeviceState() {
		return mDeviceState;
	}

	public LiveData<Boolean> getLEDState() {
		return mLEDState;
	}

	public LiveData<Boolean> isSupported() {
		return mIsSupported;
	}

	public GatewayViewModel(@NonNull final Application application) {
		super(application);

		// Initialize the manager
		gatewayManager = new GatewayManager(getApplication());
		gatewayManager.setGattCallbacks(this);
	}

	/**
	 * Connect to peripheral.
	 */
	public void connect(@NonNull final DiscoveredBluetoothDevice device) {
		// Prevent from calling again when called again (screen orientation changed)
		if (mDevice == null) {
			ringos = new ArrayList<>();
			ringos.add(new RingoDevice("Gateway", -1, null, "100%", "none", "RFID: none", 0));
			mDevice = device.getDevice();
			reconnect();
		}
	}

	/**
	 * Reconnects to previously connected device.
	 * If this device was not supported, its services were cleared on disconnection, so
	 * reconnection may help.
	 */
	public void reconnect() {
		if (mDevice != null) {
			gatewayManager.connect(mDevice)
					.retry(3, 100)
					.useAutoConnect(false)
					.enqueue();
		}
	}

	public ArrayList<RingoDevice> getRingos() {
		return ringos;
	}

	/**
	 * Disconnect from peripheral.
	 */
	public void disconnect() {
		mDevice = null;
		gatewayManager.disconnect().enqueue();
	}

	public void toggleLed(int connHandle, boolean isOn) {
		gatewayManager.send(connHandle, isOn);
		mLEDState.setValue(isOn);

	}

	@Override
	protected void onCleared() {
		super.onCleared();
		if (gatewayManager.isConnected()) {
			disconnect();
		}
	}

	@SuppressWarnings("ConstantConditions")
	@Override
	public void onDataUpdate(@NonNull BluetoothDevice device, @NonNull  Data data) {
		int type = data.getIntValue(Data.FORMAT_UINT8, 0);
		// connection handle 00 01 gets read as 01 00 = 256.
		int connHandle = data.getIntValue(Data.FORMAT_UINT16, 1);
		boolean pressed = false;
		switch (type) {
			case 1:
				int name = data.getIntValue(Data.FORMAT_UINT24, 6);
				RingoDevice ringo = new RingoDevice("Disc " + connHandle, connHandle);
				if (!ringos.contains(ringo)) {
					ringos.add(ringo);
					mDeviceState.postValue(true);
				}
				break;
			case 2:
				for ( RingoDevice r : ringos ) {
					if (r.getConnHandle() == connHandle) {
						ringos.remove(r);
						mDeviceState.postValue(false);
						break;
					}
				}
				break;
			case 3:
				int index = 0;
				for ( RingoDevice r : ringos ) {
					if ( r.getConnHandle() == connHandle ) {
						index = ringos.indexOf(r);
					}
				}
				if (data.getIntValue(Data.FORMAT_UINT8, 4) == 1) {
					pressed = true;
					ringos.get(index).count();
				}
				ringos.get(index).setSensor(pressed ? "Pressed" : "Released");
				mButtonState.postValue(pressed);
				break;
		}

//		mButtonState.postValue(pressed);
	}

//	@Override
//	public void onDeviceConnectionNotification(@NonNull BluetoothDevice device, boolean connected) {
//		mDeviceState.postValue(connected);
//	}

	@Override
	public void onLedStateChanged(@NonNull final BluetoothDevice device, final boolean on) {
		mLEDState.postValue(on);
	}

	@Override
	public void onDeviceConnecting(@NonNull final BluetoothDevice device) {
		mConnectionState.postValue(getApplication().getString(R.string.state_connecting));
	}

	@Override
	public void onDeviceConnected(@NonNull final BluetoothDevice device) {
		mIsConnected.postValue(true);
		mConnectionState.postValue(getApplication().getString(R.string.state_discovering_services));
	}

	@Override
	public void onDeviceDisconnecting(@NonNull final BluetoothDevice device) {
		mIsConnected.postValue(false);
	}

	@Override
	public void onDeviceDisconnected(@NonNull final BluetoothDevice device) {
		mIsConnected.postValue(false);
	}

	@Override
	public void onLinkLossOccurred(@NonNull final BluetoothDevice device) {
		mIsConnected.postValue(false);
	}

	@Override
	public void onServicesDiscovered(@NonNull final BluetoothDevice device,
									 final boolean optionalServicesFound) {
		mConnectionState.postValue(getApplication().getString(R.string.state_initializing));
	}

	@Override
	public void onDeviceReady(@NonNull final BluetoothDevice device) {
		mIsSupported.postValue(true);
		mConnectionState.postValue(null);
		mOnDeviceReady.postValue(null);
	}

	@Override
	public void onBondingRequired(@NonNull final BluetoothDevice device) {
		// Blinky does not require bonding
	}

	@Override
	public void onBonded(@NonNull final BluetoothDevice device) {
		// Blinky does not require bonding
	}

	@Override
	public void onBondingFailed(@NonNull final BluetoothDevice device) {
		// Blinky does not require bonding
	}

	@Override
	public void onError(@NonNull final BluetoothDevice device,
                        @NonNull final String message, final int errorCode) {
		// TODO implement
	}

	@Override
	public void onDeviceNotSupported(@NonNull final BluetoothDevice device) {
		mConnectionState.postValue(null);
		mIsSupported.postValue(false);
	}
}
