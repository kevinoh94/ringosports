package com.example.ringosports.views.device

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ringosports.R
import com.example.ringosports.adapters.DeviceListAdapter
import com.example.ringosports.adapters.TraineeListAdapter
import com.example.ringosports.bluetooth.device.manager.GatewayViewModel
import com.example.ringosports.bluetooth.scanning.DiscoveredBluetoothDevice
import com.example.ringosports.models.RingoDevice
import com.example.ringosports.models.Trainee

class DeviceListFragment : Fragment() {
    private lateinit var rootView: View
    private lateinit var colorAdapter: ArrayAdapter<String>
    private lateinit var mViewModel: GatewayViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: DeviceListAdapter
    private lateinit var ringoDevices: ArrayList<RingoDevice>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.recycler_view_frag, container, false).apply { tag = TAG }

        val gateway: DiscoveredBluetoothDevice = activity!!.intent.getParcelableExtra("gateway")
        mViewModel = ViewModelProviders.of(this).get(GatewayViewModel::class.java)
        mViewModel.connect(gateway)

        ringoDevices = mViewModel.ringos

        // Initialize colorAdapter
        val colors = arrayOf("Red", "Orange", "Yellow", "Green", "Blue", "Violet")
        colorAdapter = ArrayAdapter(rootView.context, R.layout.spinner_item, colors)


        // Set Up device list view
        recyclerView = rootView.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = GridLayoutManager(rootView.context, 2)
        adapter = DeviceListAdapter(this, ringoDevices)
        recyclerView.adapter = adapter

        mViewModel.buttonState.observe(this, Observer<Boolean>{
            adapter.notifyDataSetChanged()
        })

        mViewModel.deviceState.observe(this, Observer<Boolean>{
            adapter.notifyDataSetChanged()
        })

        return rootView
    }

    override fun onDestroy() {
        super.onDestroy()
        mViewModel.disconnect()
    }

    fun setColorAdapter(device : RingoDevice) {
        device.colorAdapter = colorAdapter
    }

    fun sendLed(connHandle: Int, on: Boolean) {
        mViewModel.toggleLed(connHandle, on)
    }

    companion object {
        private val TAG = "RecyclerViewFragment"
    }
}