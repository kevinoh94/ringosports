package com.example.ringosports.views.results

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.ringosports.R
import com.example.ringosports.adapters.TraineeListAdapter
import com.example.ringosports.models.Trainee

class ResultsListActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var trainees: ArrayList<Trainee>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results_list)

        recyclerView = findViewById(R.id.results_list)
        initTrainees()
        recyclerView.adapter = TraineeListAdapter(this, trainees, true)

    }

    private fun initTrainees() {
        trainees = ArrayList()
        trainees.add(createTrainee("Fred Wheeler", "0014"))
        trainees.add(createTrainee("Lewis Jordan", "0009"))
        trainees.add(createTrainee("Zackary Denzil", "untagged"))
        trainees.add(createTrainee("Ryder Power", "untagged"))
        trainees.add(createTrainee("Beverly Chen", "0004"))
    }

    private fun createTrainee(name: String, tag: String): Trainee {
        val builder = StringBuilder()
        builder.append("Name: ").append(name)
        builder.append("\nRFID: ").append(tag)
        builder.append("\nDisc 1: 00:02, 00:59, 02:03")
        builder.append("\nDisc 2: 00:09, 01:11, 02:10")
        builder.append("\nDisc 3: 00:32, 01:41, 02:19")
        builder.append("\nDisc 4: 00:48, 01:52, 02:55")

        return Trainee(name, tag, builder.toString())
    }
}