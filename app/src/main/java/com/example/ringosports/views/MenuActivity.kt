package com.example.ringosports.views

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import com.example.ringosports.R
import com.example.ringosports.bluetooth.device.manager.callback.RingoLED
import com.example.ringosports.views.device.ScannerActivity
import com.example.ringosports.views.results.ResultsListActivity
import com.example.ringosports.views.session.SessionSelectActivity
import com.example.ringosports.views.trainee.TraineeManagementActivity
import no.nordicsemi.android.ble.data.Data

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val train = findViewById<Button>(R.id.train)
        val results = findViewById<Button>(R.id.results)
        val setting = findViewById<Button>(R.id.setting)
        val management = findViewById<Button>(R.id.management)

        train.setOnClickListener {
            val intent = Intent(this, SessionSelectActivity::class.java)
            startActivity(intent)
        }

        results.setOnClickListener {
            val intent = Intent(this, ResultsListActivity::class.java)
            startActivity(intent)
        }

        setting.setOnClickListener {
            val intent = Intent(this, ScannerActivity::class.java)
            startActivity(intent)
        }

        management.setOnClickListener {
            val intent = Intent(this, TraineeManagementActivity::class.java)
            startActivity(intent)
        }
    }
}