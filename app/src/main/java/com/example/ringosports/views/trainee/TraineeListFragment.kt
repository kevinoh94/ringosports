package com.example.ringosports.views.trainee

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ringosports.R
import com.example.ringosports.adapters.TraineeListAdapter
import com.example.ringosports.models.Trainee

class TraineeListFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var trainees: ArrayList<Trainee>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTrainees()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.recycler_view_frag, container, false).apply { tag = TAG }
        recyclerView = rootView.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(rootView.context)

        recyclerView.adapter = TraineeListAdapter(null ,trainees, false)

        return rootView
    }

    private fun initTrainees() {
        trainees = ArrayList<Trainee>()
        val trainee1 = Trainee("Fred Wheeler", "0014", null)
        trainees.add(trainee1)
        val trainee2 = Trainee("Lewis Jordan","0009", null )
        trainees.add(trainee2)
        trainees.add(Trainee("Zackary Denzil", "untagged", null))
        trainees.add(Trainee("Ryder Power", "untagged", null))
        trainees.add(Trainee("Beverly Chen", "0004", null))
    }

    companion object {
        private val TAG = "RecyclerViewFragment"
    }
}