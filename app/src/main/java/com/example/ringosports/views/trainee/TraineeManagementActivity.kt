package com.example.ringosports.views.trainee

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.ringosports.R
import com.example.ringosports.views.results.ResultsDetailFragment

class TraineeManagementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trainee_management)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().run {
                replace(R.id.trainee_list_fragment, TraineeListFragment())
                commit()
            }
        }
    }
}