package com.example.ringosports.views.results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.ringosports.R
import com.example.ringosports.models.Trainee
import kotlinx.android.synthetic.main.results_detail.view.*

class ResultsDetailFragment : Fragment() {

    private var trainee: Trainee? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let{
            if (it.containsKey(ARG_TRAINEE_ID)) {
                trainee = it.getSerializable(ARG_TRAINEE_ID) as Trainee
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.results_detail, container, false)
        trainee?.let {
            rootView.results_detail.text = it.results
        }

        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_TRAINEE_ID = "trainee_id"
    }

}