package com.example.ringosports.views.device

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.ringosports.R
import com.example.ringosports.bluetooth.scanning.DiscoveredBluetoothDevice

class DeviceManagementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_device_management)

        val gateway = intent.getParcelableExtra<DiscoveredBluetoothDevice>("gateway")
        intent.putExtra("gateway", gateway)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().run {
                replace(R.id.device_list_fragment, DeviceListFragment())
                commit()
            }
        }
    }
}