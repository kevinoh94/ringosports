package com.example.ringosports.views.device

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.ringosports.R
import com.example.ringosports.bluetooth.scanning.DevicesAdapter
import com.example.ringosports.bluetooth.scanning.DiscoveredBluetoothDevice
import com.example.ringosports.bluetooth.scanning.ScannerStateLiveData
import com.example.ringosports.bluetooth.scanning.ScannerViewModel
import com.example.ringosports.bluetooth.utils.Utils
import kotlinx.android.synthetic.main.activity_scanner.*
import kotlinx.android.synthetic.main.info_no_bluetooth.*
import kotlinx.android.synthetic.main.info_no_devices.*
import kotlinx.android.synthetic.main.info_no_permission.*

class ScannerActivity : AppCompatActivity(), DevicesAdapter.OnItemClickListener {
    private val REQUEST_ACCESS_COARSE_LOCATION: Int = 1108 // random number

    private lateinit var mScannerViewModel: ScannerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.app_name)

        mScannerViewModel = ViewModelProviders.of(this).get(ScannerViewModel::class.java)
        mScannerViewModel.scannerState.observe(this, Observer<ScannerStateLiveData> { startScan(it) })

        val adapter = DevicesAdapter(this, mScannerViewModel.devices)
        adapter.setOnItemClickListener(this)
        recycler_view_ble_devices.adapter = adapter

        /**
         * Permissions
         */
        action_enable_location.setOnClickListener {
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
        action_enable_bluetooth.setOnClickListener {
            val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivity(intent)
        }
        action_grant_location_permission.setOnClickListener {
            Utils.markLocationPermissionRequested(this)
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_ACCESS_COARSE_LOCATION)

        }
        action_permission_settings.setOnClickListener {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.data = Uri.fromParts("package", packageName, null)
            startActivity(intent)
        }

    }

    override fun onRestart() {
        super.onRestart()
        clear()
    }

    override fun onStop() {
        super.onStop()
        stopScan()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.filter, menu)
        menu?.findItem(R.id.filter_nearby)!!.setChecked(mScannerViewModel.isNearbyFilterEnabled)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.filter_nearby -> {
                item.setChecked(!item.isChecked)
                mScannerViewModel.filterByDistance(item.isChecked)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(device: DiscoveredBluetoothDevice) {
        val intent = Intent(this, DeviceManagementActivity::class.java)
        intent.putExtra("gateway", device)
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ACCESS_COARSE_LOCATION -> {
                mScannerViewModel.refresh()
                return
            }
        }
    }

    /**
     * Start scanning for Bluetooth devices or displays a message based on the scanner state
     */
    private fun startScan(state: ScannerStateLiveData) {
        // First, check the Location permission. This is required on Marshmallow onwards in order
        // to scan for Bluetooth LE devices.
        if (Utils.isLocationPermissionsGranted(this)) {
            no_location_permission.visibility = View.GONE

            // Bluetooth must be enabled
            if (state.isBluetoothEnabled) {
                bluetooth_off.visibility = View.GONE

                // We are now OK to start scanning
                mScannerViewModel.startScan()
                state_scanning.visibility = View.VISIBLE

                if (!state.hasRecords()) {
                    no_devices.visibility = View.VISIBLE

                    if (!Utils.isLocationRequired(this) || Utils.isLocationEnabled(this)) {
                        no_location.visibility = View.INVISIBLE
                    } else {
                        no_location.visibility = View.VISIBLE
                    }
                } else {
                    no_devices.visibility = View.GONE
                }
            } else {
                bluetooth_off.visibility = View.VISIBLE
                state_scanning.visibility = View.INVISIBLE
                no_devices.visibility = View.GONE
                clear()
            }
        } else {
            no_location_permission.visibility = View.VISIBLE
            bluetooth_off.visibility = View.GONE
            state_scanning.visibility = View.INVISIBLE
            no_devices.visibility = View.GONE

            val deniedForever: Boolean = Utils.isLocationPermissionDeniedForever(this)
            if (deniedForever) {
                action_grant_location_permission.visibility = View.VISIBLE
                action_permission_settings.visibility = View.GONE
            } else {
                action_grant_location_permission.visibility = View.GONE
                action_permission_settings.visibility = View.VISIBLE
            }
        }
    }

    private fun stopScan() { mScannerViewModel.stopScan() }

    private fun clear() {
        mScannerViewModel.devices.clear()
        mScannerViewModel.scannerState.clearRecords()
    }
}