package com.example.ringosports.views.session

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.example.ringosports.R

class SessionSelectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session_select)

        val trainers = arrayOf("Greg", "Paul", "Brian")
        val sessions = arrayOf("T-section", "3-way loop", "Fast Five");

        val trainerSpinner = findViewById<Spinner>(R.id.trainer)
        val sessionSpinner = findViewById<Spinner>(R.id.session)

        val arrayAdapter = ArrayAdapter(this, R.layout.spinner_item, trainers)
        trainerSpinner.adapter = arrayAdapter

        val arrayAdapter2 = ArrayAdapter(this, R.layout.spinner_item, sessions)
        sessionSpinner.adapter = arrayAdapter2
    }
}